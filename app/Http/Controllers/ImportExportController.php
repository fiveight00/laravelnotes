<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use Excel;

class ImportExportController extends Controller
{
    public function import(){
        return view('csv.import');
    }

    public function download($type) {

        $data = Post::get(['title','body'])->toArray();
        return Excel::create('notes', function($excel) use ($data) {
            $excel->sheet('mySheet', function($sheet) use ($data)
            {
                $sheet->fromArray($data);
            });
        })->download($type);

    }

    public function myImport(Request $request) {

        if($request->file('import_file')) {
            $path = $request->file('import_file')->getRealPath();
            $data = Excel::load($path, function($reader)
            {
            })->get();

            if(!empty($data) && $data->count())
            {
                foreach ($data->toArray() as $row)
                {
                    if(!empty($row))
                    {
                        $dataArray[] =
                            [
                                'title' => $row['title'],
                                'body' => $row['body'],
                            ];
                    }
                }
                if(!empty($dataArray))
                {
                    Post::insert($dataArray);
                    return back();
                }
            }
        } else {
            return redirect('/csv/import');
        }
    }
}
