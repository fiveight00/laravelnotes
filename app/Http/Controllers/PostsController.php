<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Post;
use App\Picture;
use Excel;
use Storage;
use App\Http\Requests\StoreNotePost;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class PostsController extends Controller
{

    public function index() {
        $posts = Post::all();
        $pictures = Picture::all();
        return view('index', compact('posts', 'pictures'));
    }

    public function show(Post $post) {
        return view('posts.show', compact('post'));
    }

    public function create() {
        return view("posts.create");
    }

    public function store(Post $post, StoreNotePost $request) {

        $names = array();

        if (Input::hasFile('files')) {
            $all_uploads = Input::file('files');

            if (!is_array($all_uploads)) {
                $all_uploads = array($all_uploads);
            }
            foreach ($all_uploads as $upload) {
                $validator = Validator::make(
                    array('file' => $upload),
                    array('file' => 'required|mimes:jpeg,png|image|max:1000')
                );
                if ($validator->passes()) {

                    $picName = str_random(8).'.png';
                    $names[] = $picName;

                    Storage::put('public/images/' . $picName, file_get_contents($upload));

                } else {
                    $error_messages[] = 'File "' . $upload->getClientOriginalName() . '":' . $validator->messages()->first('file');
                    return $error_messages;
                }
            }
        }
        $created = Post::create(
            request(array('title', 'body'))
        );

        $id = $created->id;
        foreach ($names as $name) {
            Picture::create(['id' => $id, 'name' => $name]);
        }

        return redirect('/');
    }

    public function edit(Post $post) {
        return view("posts.edit", compact('post'));
    }

    public function update(Post $post, StoreNotePost $request ){

        $post->update(request(['title', 'body']));

        $countPicDel = 5;
        for ($i = 1; $i <= $countPicDel; $i++) {
            $check = request('hidden'.$i);
            if (!empty($check)) {
                unlink(storage_path('app/public/images/' . $check));
                Picture::where('name', $check)->delete();
            }
        }

        if (Input::hasFile('files')) {

            $all_uploads = Input::file('files');

            if (!is_array($all_uploads)) {
                $all_uploads = array($all_uploads);
            }
            foreach ($all_uploads as $upload) {
                $validator = Validator::make(
                    array('file' => $upload),
                    array('file' => 'required|mimes:jpeg,png|image|max:1000')
                );
                if ($validator->passes()) {
                    $picName = str_random(8).'.png';
                    Storage::put('public/images/'. $picName, file_get_contents($upload));
                    Picture::create(['id' => $post->id, 'name' => $picName]);
                } else {
                    $error_messages[] = 'File "' . $upload->getClientOriginalName() . '":' . $validator->messages()->first('file');
                    return $error_messages;
                }
            }
        }
        return redirect('/');
    }

    public function destroy(Post $post) {
        $pictures = Post::find($post->id);
        foreach ($pictures->names as $name) {
            unlink(storage_path('app/public/images/'.$name->name));
            Picture::where('name', $name->name)->delete();
        }
        $post->delete();
        return redirect('/');
    }

    public function deletePicture(Post $post) {
        return redirect('/');
    }
}
