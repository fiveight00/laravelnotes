@extends('layouts.layout')

@section('content')
    <div class="row">

        <div class="col-sm-8 blog-main">

            <div class="blog-post">
                <h2 class="blog-post-title">{{$post->title}}</h2>
                <div class="showDiv">
                    <p>
                        {!!$post->body!!}
                    </p>
                </div>
                        @foreach ($pictures= App\Post::find($post->id)->names as $name)
                            <img class='showImg' src='{{URL::to("/storage/images/$name->name")}}'>
                        @endforeach
            </div>
        </div>
@endsection