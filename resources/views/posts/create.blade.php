@extends('layouts.layout')

@section('content')
    <h2>Новая заметка</h2>

    <form action="/post" method="post" enctype="multipart/form-data">

        {{csrf_field()}}

        <div class="form-group">
            <label for="title">Заголовок</label>
            <input class="form-control" type="text" name="title" id="title">
        </div>

        <div class="form-group">
            <label for="body">Содержание</label>
            <textarea class="form-control" name="body" id="body"></textarea>
        </div>

        <input type="hidden" name="time" value="">

        <div class="form-group myInputs">
            <label for="files[]">Формат jpeg/png. Размер не должен превышать 1 mb.</label>
            <input type="file" name="files[]">
        </div>

        <div class="form-group myLinkDiv">
            <a href="#" class="myLink">Еще</a>
        </div>

        <div class="form-group">
            <button class="btn btn-primary" type="submit">Сохранить</button>
        </div>

        @include('layouts.error')

    </form>


@endsection