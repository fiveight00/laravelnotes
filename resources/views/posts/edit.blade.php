@extends('layouts.layout')

@section('content')
    <h2>Редактировать заметку</h2>

    <form action="/posts/{{$post->id}}" method="post" enctype="multipart/form-data">

        {{csrf_field()}}

        {!! method_field('patch') !!}

        <div class="form-group">
            <label for="title">Заголовок</label>
            <input class="form-control" type="text" name="title" id="title" value="{{$post->title}}">
        </div>

        <div class="form-group">
            <label for="body">Текст</label>
            <textarea class="form-control" name="body" id="body">{{$post->body}}</textarea>
        </div>

        <input type="hidden" value="" name="hidden1" id="hidden1">
        <input type="hidden" value="" name="hidden2" id="hidden2">
        <input type="hidden" value="" name="hidden3" id="hidden3">
        <input type="hidden" value="" name="hidden4" id="hidden4">
        <input type="hidden" value="" name="hidden5" id="hidden5">


        @foreach ($pictures=App\Post::find($post->id)->names as $name)
                <div>
                <img class='editImg' src='{{URL::to("/storage/images/$name->name")}}'>
                <input type='hidden' name='pictureName' value='{{$name->name}}'>
                <button class='btn btn-danger btnDelete editBtnDelete' type='button'>Удалить</button></div>
        @endforeach

        <div class="form-group myInputs">
            <label for="files[]">Формат jpeg/png. Размер не должен превышать 1 mb.</label>
            <input type="file" name="files[]">
        </div>

        <div class="form-group myLinkDiv">
            <a href="#" class="myLinkEdit">Еще</a>
        </div>

        <div class="form-group">
            <button class="btn btn-primary" type="submit">Обновить</button>
        </div>

        @include('layouts.error')

    </form>


@endsection