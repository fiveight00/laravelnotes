<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Laravel Notes</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <link rel="stylesheet" href="{{ URL::to('css/navbar.css') }}">
    <link rel="stylesheet" href="{{ URL::to('css/layoutBlade.css') }}">
    <link rel="stylesheet" href="{{ URL::to('css/showBlade.css') }}">
    <link rel="stylesheet" href="{{ URL::to('css/editBlade.css') }}">
    <link rel="stylesheet" href="{{ URL::to('css/indexBlade.css') }}">

    <script src="{{ URL::to('tinymce/js/tinymce/jquery.tinymce.min.js') }}"></script>
    <script src="{{ URL::to('tinymce/js/tinymce/tinymce.min.js') }}"></script>
    <script>tinymce.init({ selector:'textarea' });</script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>


</head>
<body>
@include('layouts.headerNavigation')
<div class="jumbotron">
    <div class="container">
        <h1>Laravel notes</h1>
        <p>Приложение Заметки</p>
    </div>
</div>

<div class="container">
    @yield('content')
</div>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script src="{{ URL::to('js/editBlade.js') }}"></script>
<script src="{{ URL::to('js/createBlade.js') }}"></script>
</body>
</html>