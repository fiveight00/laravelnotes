@extends('layouts.layout')

@section('content')
    <div class="row">
        <table class="table">
            <thead>
            <tr>
                <th>№</th>
                <th>Название</th>
                <th>Содержание</th>
                <th>Фото</th>
                <th>Действия</th>
            </tr>
            </thead>
            <tbody>
        <?php $number=0; ?>
        @foreach($posts as $post)

            <tr>
                <th class="thId thInner"><?php echo ++$number . ") "?></th>
                <th class="thTitle thInner">{{ $post->title }}</th>
                <th class="thBody thInner">{!! mb_strimwidth($post->body, 0, 200, "...")!!}</th>
                <th class="thImg thInner">

                            @foreach ($pictures = App\Post::find($post->id)->names as $name)
                                <img class='indexImg' src='{{URL::to("/storage/images/$name->name")}}'>
                            @endforeach

                </th>
                <th class="thOptions thInner">
                    <a href="/posts/{{$post->id}}" class="btn btn-success btnGreen">Открыть</a>
                    <a href="/posts/{{$post->id}}/edit" class="btn btn-primary btnBlue">Редактировать</a>
                    <form class="indexForm" action="/posts/{{$post->id}}" method="post">
                        {{csrf_field()}}
                        {!! method_field('delete') !!}
                        <button type="submit" class="btn btn-danger btnRed">Удалить</button>
                    </form>
                </th>
            </tr>
        @endforeach
            </tbody>
        </table>
    </div>
@endsection