@extends('layouts.layout')

@section('content')

    <div class="container">
        <br />
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-6">
                <div class="row">
                    <form action="{{ URL::to('importExcel') }}" method="post" enctype="multipart/form-data">
                        <div class="col-md-6">
                            {{csrf_field()}}
                            <input type="file" name="import_file"/>
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-primary" type="submit">Импортировать</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-2">
            </div>
        </div>
    </div>

@endsection