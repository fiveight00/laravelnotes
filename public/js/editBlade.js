$(document).ready(function() {
    $n = $('img').length;
    if ($n===4) {
        $(".myLinkDiv").remove();
    }
    if ($n===5) {
        $(".myInputs").remove();
        $(".myLinkDiv").remove();
    }

    $('body').on('click', '.myLinkEdit', function () {
        if ($n < 3) {
            ++$n;
            $(".myInputs:last").after("<div class='form-group myInputs'><input type='file' name='files[]'></div>");
        } else if ($n === 3) {
            $(".myLinkDiv").remove();
            $(".myInputs:last").after("<div class='form-group myInputs'><input type='file' name='files[]'></div>");
        }
    });
});

$check=0;
$(".btnDelete").click( function(){
    $check++;
    $val = $(this).parent().find('input').val();
    $('#hidden'+$check).val($val);
    $('.deleteNote').hide();
    $(this).parent().replaceWith("<h5 class='deleteNote'>Для применения изменений, нажмите Обновить</h5>");
});